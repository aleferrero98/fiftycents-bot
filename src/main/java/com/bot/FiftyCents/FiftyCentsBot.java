package com.bot.FiftyCents;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.channel.concrete.TextChannel;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.requests.GatewayIntent;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FiftyCentsBot extends ListenerAdapter {
   private static final String CHANNEL_ID = "848640620670353460";
   private static JDA jda;
   private String lastValue;

   public static void create() {
      log.info("Inicializando Bot...");
      String message = "Que onda pobre?";
      try {
         message = "$ " + getDollarBlueExchange();
      } catch (Exception ex) {}

      jda = JDABuilder.createDefault(System.getenv("TOKEN"))
                      .setActivity(Activity.playing(message))
                      .enableIntents(GatewayIntent.MESSAGE_CONTENT)
                      .addEventListeners(new FiftyCentsBot())
                      .build();
   }

   @Scheduled(cron = "0 0/10 10-18 * * *")
   public void sendDataToSpamChannelWhenMarketsOpened() {
      log.info("Mandando cotizacion...");
      TextChannel txtChannel = jda.getTextChannelById(CHANNEL_ID);
      String message = "Oh shit! I don't know how much fifty cents is in ARS!";

      try {
         Double fiftyCentsInARS = Double.parseDouble(getDollarBlueExchange().replace(",", ".")) / 2.0;
         message = "$ " + fiftyCentsInARS;
         lastValue = message;
      } catch (Exception ex) {}

      if (Objects.nonNull(txtChannel) && txtChannel.canTalk()) {
         txtChannel.sendMessage(message).queue();
      } else {
         log.error("Error al enviar mensaje al canal");
      }
   }

   @Scheduled(cron = "0 0/20 0-9,19-23 * * *")
   public void sendDataToSpamChannelWhenMarketsClosed() {
      log.info("Mandando cotizacion...");
      TextChannel txtChannel = jda.getTextChannelById(CHANNEL_ID);
      String message = lastValue;

      try {
         Double fiftyCentsInARS = Double.parseDouble(getDollarBlueExchange().replace(",", ".")) / 2.0;
         message = "$ " + fiftyCentsInARS;
         lastValue = message;
      } catch (Exception ex) {}

      if (Objects.nonNull(txtChannel) && txtChannel.canTalk()) {
         txtChannel.sendMessage(message).queue();
      } else {
         log.error("Error al enviar mensaje al canal");
      }
   }

   //   @Override
   //   public void onMessageReceived(MessageReceivedEvent event) {
   //      System.out.println("Recibo mensaje de " + event.getAuthor().getName() + ": " + event.getMessage().getContentDisplay());
   //      if (event.getAuthor().isBot()) {
   //         return;
   //      }
   //      if (event.getMessage().getContentRaw().equalsIgnoreCase("/dolarito")) {
   //         event.getChannel().sendMessage("Hola que tal").queue();
   //      }
   //   }

   public static String getDollarBlueExchange() {
      RestTemplate restTemplate = new RestTemplate();
      String result = restTemplate.getForObject("https://www.infodolar.com/", String.class);

      String[] resultArr = result.split("<td class=\"colCompraVenta\" data-order=\"\\$ ");
      List<String> quotationsList = Arrays.asList(Arrays.copyOfRange(resultArr, 1, resultArr.length + 1));
      quotationsList = quotationsList.stream().map(x ->  (Objects.nonNull(x) && x.split("\">").length > 1)? x.split("\">")[0] : "")
                                     .collect(Collectors.toList());

      log.info(quotationsList.toString());

      return quotationsList.get(1); //tomo el valor de venta
   }
}
