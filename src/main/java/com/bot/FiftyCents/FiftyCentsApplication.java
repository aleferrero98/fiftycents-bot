package com.bot.FiftyCents;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class FiftyCentsApplication {

	public static void main(String[] args) {
		FiftyCentsBot.create();
		SpringApplication.run(FiftyCentsApplication.class, args);
	}

}
